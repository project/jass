A Drupal theme based on the utility-first CSS Framework JASS: https://github.com/diqidoq/jass.

JASS · _"play with Jasm CSS classes conducted by SASS."_

<sup>Development started in 2012 formerly named QCSS (with LESS), first released in 2015 and refactored to JASS (with SASS) in 2023.</sup><br>
<sup>Supported by www.maroqqo.com</sup>

    >  Stop learning frameworks. Stop hacking styles. Start building web apps. 👍</blockquote>

*JASS:* A simple utility first type of _"describe your style as class"_ CSS framework with no stylesheet editing and no recompiling for new themes in mind. Development started in 2012 formerly named QCSS. Refactored in 2023 and renamed to JASS. Being cross browser compatible down to 2009 while using SASS (SCSS) for the generator (not required by default). 

An easy out-of-the-box styling layer from the bottom up by using a creative class group naming approach, easy to learn and to reuse for each project, with a footprint of only 60kb by default settings. And with inheritance support for child elements, especially good for content management framework themes, where inner elements are sometimes hard to reach.

Supported class naming components:
----------------------------------
 - borders (width, radius, directions)
 - colors (extensive websafe color codes)
 - responsive layouts (without flex and grid to be accessible all over the world)
 - shadows
 - spacing tools (choose your rythm)
 - typography (most extensive typo classes available in web)
 - tools (anything needed in combination with other components like floats, positions, order, etc.)
 - optional override-switch per component (!important), default: no. (off)
 - optional child target setting per component (classes with trailing --c), default: no. (off)
 - optional print style setting per component (classes with trailing --p), default: no. (off)

Issues and Releases
-------------------
Releases will be semtantic and based on latest Drupal version supported. Most of the releases will hopefullly also support the previous Drupal version. 

So JASS 11.1 will support Drupal 11 and 10. JASS 12.1 will support 12 and 11.

Issues should be reported by giving details about the version used but with forking of latest dev branch of this version in mind for better tracking and merging fixes into new releases.

So for example: please report a 11.1 issue with selecting version 11.1.x-dev on top but with making a note in the summary that the issue report is against 11.1 in the issue creation process. 

It would also help if you could test if this issue persists in latest branch of this version and report it in the summary.

Sources
-------
- Issues hosted on Github: https://github.com/diqidoq/jass.
- Drupal theme project info and release page: http://www.drupal.org/project/jass